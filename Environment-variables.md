# **DRAFT**
## Configuration

Application requires the following variables to be set. As an alternative to setting the environment variables, the respective lines in `application.properties` can be adjusted, or Java runtime parameters may be passed.


* `CONTAINER_API`
* `DB`
* `DB_HOST`
* `DB_SECRET`
* `DB_USER`
* `KC_HOST`
* `KC_PORT`
* `KC_REALM`
* `KC_RESOURCE`
* `LOGLEVEL`

`LOGLEVEL` is optional and defaults to `INFO`. The other values are mandatory, the application will fail to operate without them.